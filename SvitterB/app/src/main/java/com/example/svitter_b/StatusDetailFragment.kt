package com.example.svitter_b

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.example.svitter_b.databinding.FragmentStatusDetailBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.type.LatLng
import java.io.File
import java.io.IOException

class StatusDetailFragment : Fragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentStatusDetailBinding

    private lateinit var firebaseStorage: FirebaseStorage

    private val dbStatuses = FirebaseDatabase.getInstance("https://svitter-b-default-rtdb.europe-west1.firebasedatabase.app/").getReference(NODE_STATUS)

    private lateinit var storageReference: StorageReference

    private lateinit var databaseReference: DatabaseReference

    private lateinit var mapView: MapView

    private val args: StatusDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentStatusDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseStorage = FirebaseStorage.getInstance()

        databaseReference = dbStatuses

        storageReference = firebaseStorage.getReference(NODE_IMAGE)

        val clickedStatusId = args.statusid

        mapView = binding.googleMap

        mapView.getMapAsync(this)
        mapView.onCreate(savedInstanceState)

        databaseReference.child(clickedStatusId).get().addOnCompleteListener {
            if(it.isSuccessful){
                if(it.result.exists()){
                    Toast.makeText(requireContext(), "Data retrieved successfully", Toast.LENGTH_LONG).show()
                    val dataSnapshot = it.result
                    val chat = dataSnapshot.child("chat").getValue()
                    val email = dataSnapshot.child("email").getValue()
                    val imagePath = dataSnapshot.child("imagepath").getValue()
                    val lat = dataSnapshot.child("lat").getValue()
                    val lon = dataSnapshot.child("lon").getValue()

                    binding.tvUsernameDet.text = email.toString()
                    binding.tvChatDet.text = chat.toString()
                    if(imagePath != null){
                        val imageReference = storageReference.child(imagePath.toString())
                        val localFile: File = File.createTempFile("tempfile",".jpg")
                        try {
                            imageReference.getFile(localFile).addOnSuccessListener {
                                var bitmap: Bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                                binding.iwStatusImageDet.setImageBitmap(bitmap)
                            }.addOnFailureListener{
                                binding.iwStatusImageDet.setImageResource(R.drawable.ic_launcher_background)
                            }
                        }catch (e: IOException){
                            e.printStackTrace()
                        }
                    }else{
                        binding.iwStatusImageDet.setImageResource(R.drawable.ic_launcher_foreground)
                    }
                    if(lat != null && lon != null) {
                        binding.tvLat.text = lat.toString()
                        binding.tvLon.text = lon.toString()
                    }
                }else{
                    Toast.makeText(requireContext(), "Status data doesn't exist", Toast.LENGTH_LONG).show()
                }
            }else{
                Toast.makeText(requireContext(), "Can't retrieve status data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {

        val clickedStatusId = args.statusid

        databaseReference.child(clickedStatusId).get().addOnCompleteListener {
            if(it.isSuccessful){
                if(it.result.exists()){
                    Toast.makeText(requireContext(), "Data retrieved successfully", Toast.LENGTH_LONG).show()
                    val dataSnapshot = it.result
                    val lat = dataSnapshot.child("lat").getValue()
                    val lon = dataSnapshot.child("lon").getValue()
                    if(lat != null && lon != null) {
                        val marker = com.google.android.gms.maps.model.LatLng(
                            lat.toString().toDouble(),
                            lon.toString().toDouble()
                        )
                        map.addMarker(MarkerOptions().position(marker).title("Status position"))
                    }
                }else{
                    Toast.makeText(requireContext(), "Status data doesn't exist", Toast.LENGTH_LONG).show()
                }
            }else{
                Toast.makeText(requireContext(), "Can't retrieve status data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }




}