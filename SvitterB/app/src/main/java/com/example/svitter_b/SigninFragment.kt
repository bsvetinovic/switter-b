package com.example.svitter_b

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.svitter_b.databinding.FragmentLoginBinding
import com.example.svitter_b.databinding.FragmentSigninBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class SigninFragment : Fragment() {

    private lateinit var binding: FragmentSigninBinding

    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSigninBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()

        progressDialog = ProgressDialog(requireContext())
        progressDialog.setTitle("Please wait")
        progressDialog.setCanceledOnTouchOutside(false)

        binding.tvsilogin.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.action_signinFragment_to_loginFragment)
        }

        binding.btnSignin.setOnClickListener {
            validateData()
        }
    }

    private var email = ""
    private var password = ""

    private fun validateData() {
        email = binding.etsiEmail.text.toString().trim()
        password = binding.etsiPassword.text.toString().trim()
        val password2 = binding.etsiPassword2.text.toString().trim()

        if (email.isEmpty() || Patterns.EMAIL_ADDRESS.matcher(binding.etsiEmail.toString().trim()).matches()){
            Toast.makeText(requireContext(), "Invalid email", Toast.LENGTH_LONG).show()
        }
        else if (password.isEmpty()){
            Toast.makeText(requireContext(), "Enter your password", Toast.LENGTH_LONG).show()
        }
        else if (password2.isEmpty()){
            Toast.makeText(requireContext(), "Confirm your password", Toast.LENGTH_LONG).show()
        }
        else if (password != password2){
            Toast.makeText(requireContext(), "Passwords don't match", Toast.LENGTH_LONG).show()
        }
        else{
            createNewUser()
        }
    }

    private fun createNewUser() {
        progressDialog.setMessage("Crating Account")
        progressDialog.show()

        firebaseAuth.createUserWithEmailAndPassword(email, password)

            .addOnSuccessListener {
                Toast.makeText(requireContext(), "Account has been created successfully", Toast.LENGTH_LONG).show()
                progressDialog.dismiss()
                Navigation.findNavController(binding.root).navigate(R.id.action_signinFragment_to_chatFragment)
            }
            .addOnFailureListener { e->
                progressDialog.dismiss()
                Toast.makeText(requireContext(), "Fail: ${e.message}", Toast.LENGTH_LONG).show()
            }
    }

}