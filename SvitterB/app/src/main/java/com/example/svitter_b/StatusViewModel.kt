package com.example.svitter_b

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import java.lang.Exception

class StatusViewModel: ViewModel() {

    private val dbStatuses = FirebaseDatabase.getInstance("https://svitter-b-default-rtdb.europe-west1.firebasedatabase.app/").getReference(NODE_STATUS)

    private val _status = MutableLiveData<Status>()
    val status: LiveData<Status> get() = _status

    private val _result = MutableLiveData<Exception?>()
    val result: LiveData<Exception?> get() = _result

    fun addStatus(status: Status){
        status.sid = dbStatuses.push().key

        dbStatuses.child(status.sid!!).setValue(status)
            .addOnCompleteListener {
            if (it.isSuccessful){
                _result.value = null
            }else{
                _result.value = it.exception
            }
        }

    }

    private val childEventListener = object :ChildEventListener{
        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            val status = snapshot.getValue(Status::class.java)
            status?.sid = snapshot.key
            _status.value = status!!
        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            val status = snapshot.getValue(Status::class.java)
            status?.sid = snapshot.key
            _status.value = status!!
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            val status = snapshot.getValue(Status::class.java)
            status?.sid = snapshot.key
            status?.isDeleted = true
            _status.value = status!!
        }

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
        }

        override fun onCancelled(error: DatabaseError) {
        }
    }

    fun getRealtimeUpdate(){
        dbStatuses.addChildEventListener(childEventListener)
    }

    fun updateStatus(status: Status){
        dbStatuses.child(status.sid!!).setValue(status).addOnCompleteListener {
            if (it.isSuccessful){
                _result.value = null
            }else{
                _result.value = it.exception
            }
        }
    }

    fun deleteStatus(status: Status){
        dbStatuses.child(status.sid!!).setValue(null).
        addOnCompleteListener {
            if(it.isSuccessful){
                _result.value =  null
            }else{
                _result.value = it.exception
            }
        }
    }

}