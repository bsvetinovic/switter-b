package com.example.svitter_b

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.svitter_b.databinding.FragmentAddChatBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.storage.StorageReference

class AddChatFragment : DialogFragment() {

    private lateinit var binding: FragmentAddChatBinding

    private lateinit var viewModel: StatusViewModel

    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var storageReference: StorageReference

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    var path: String? = null

    private lateinit var firebaseStorage: FirebaseStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddChatBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(StatusViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View,savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseStorage = FirebaseStorage.getInstance()

        storageReference = firebaseStorage.getReference(NODE_IMAGE)

        firebaseAuth = FirebaseAuth.getInstance()

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        val user = firebaseAuth.currentUser

        binding.tvUserAdd.text = user!!.email.toString()

        viewModel.result.observe(viewLifecycleOwner, Observer {
            val message = if (it == null) {
                getString(R.string.added_status)
            } else {
                getString(R.string.error, it.message)
            }

            Toast.makeText(requireContext(),message, Toast.LENGTH_LONG).show()
            dismiss()
        })

        binding.btnCamera.setOnClickListener {
            var iCamera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(iCamera,102)
        }

        binding.location.setOnClickListener {
            checkPermission()
        }

        binding.btnAddChat.setOnClickListener{
            val email = user.email
            val chat = binding.etChat.text.toString()

            //var locationManager: LocationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

            if(chat.trim().isEmpty()){
                binding.etChat.error = "Empty"
            }
            else {
                var status = Status()
                if(binding.latitude.text.isNotEmpty() || binding.longitude.text.isNotEmpty()) {
                    status.Lat = binding.latitude.text.toString().toDouble()
                    status.Lon = binding.longitude.text.toString().toDouble()
                }
                status.chat = chat
                status.email = email
                status.imagepath = path

                viewModel.addStatus(status)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 102){
            var bitmap: Bitmap = data!!.extras!!.get("data") as Bitmap
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            binding.iwStatusImage.setImageBitmap(bitmap)
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos)
            val imData = baos.toByteArray()
            val imagereference = storageReference.child("$timeStamp.jpg")

            var uploadTask = imagereference.putBytes(imData)
            uploadTask.addOnFailureListener{ e->
                Toast.makeText(requireContext(), "${e.message}", Toast.LENGTH_LONG).show()
            }.addOnSuccessListener{
                Toast.makeText(requireContext(), "image upload is a success:", Toast.LENGTH_LONG).show()
                path = "$timeStamp.jpg"
            }
        }
    }

    fun checkPermission(){
        val task : Task<Location> = fusedLocationProviderClient.lastLocation
        if(ActivityCompat.checkSelfPermission(requireContext(),Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(),Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101)
        }
        task.addOnSuccessListener {
            if(it != null){
                Toast.makeText(requireContext(),"${it.latitude} ${it.longitude}", Toast.LENGTH_SHORT).show()
                binding.longitude.text = it.longitude.toString()
                binding.latitude.text = it.latitude.toString()
            }
        }
    }
}