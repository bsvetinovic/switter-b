package com.example.svitter_b

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.svitter_b.databinding.FragmentUpdateChatBinding
import com.google.firebase.auth.FirebaseAuth
import java.io.*
import java.util.*

class UpdateChatFragment(val status: Status) : DialogFragment() {

    private lateinit var binding: FragmentUpdateChatBinding

    private lateinit var viewModel: StatusViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUpdateChatBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(StatusViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View,savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvUserUpdate.text = status.email
        binding.etUChat.setText(status.chat)

        binding.btnUpdateChat.setOnClickListener{
            val chat = binding.etUChat.text.toString()

            if(chat.trim().isEmpty()){
                binding.etUChat.error = "Empty"
            }
            else {
                status.chat = chat

                viewModel.updateStatus(status)
                dismiss()
                Toast.makeText(requireContext(), "Update is successful", Toast.LENGTH_LONG).show()
            }
        }
    }
}