package com.example.svitter_b

import android.location.Location
import com.google.firebase.database.Exclude

data class Status(
    @get:Exclude
    var sid: String? = null,
    var email: String? = null,
    var chat: String? = null,
    var imagepath: String? = null,
    var Lat: Double? = null,
    var Lon: Double? = null,
    @get:Exclude
    var isDeleted: Boolean = false) {

    override fun equals(other: Any?): Boolean {
        return if (other is Status){
            other.sid == sid
        }else false
    }

    override fun hashCode(): Int {
        var result = sid?.hashCode() ?: 0
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (chat?.hashCode() ?: 0)
        result = 31 * result + (imagepath?.hashCode() ?: 0)
        result = 31 * result + (Lat?.hashCode() ?: 0)
        result = 31 * result + (Lon?.hashCode() ?: 0)
        result = 31 * result + isDeleted.hashCode()
        return result
    }
}