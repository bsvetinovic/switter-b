package com.example.svitter_b

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import com.example.svitter_b.databinding.ChatCellBinding
import com.example.svitter_b.databinding.FragmentChatBinding
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File
import java.io.IOException

class StatusAdapter(): RecyclerView.Adapter<StatusAdapter.ViewHolder>() {

    private lateinit var storageReference: StorageReference

    private lateinit var firebaseStorage: FirebaseStorage

    var statuses = mutableListOf<Status>()

    var allStatuses = ArrayList<Status>()

    var onItemClick : ((Status) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        firebaseStorage = FirebaseStorage.getInstance()
        storageReference = firebaseStorage.getReference("images")
        return ViewHolder(ChatCellBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(statuses[position].imagepath != null){
            setWithImage(holder, position)
        }else {
            holder.binding.iwStatusCellImage.setImageResource(R.drawable.ic_launcher_foreground)
            holder.binding.tvUsername.text = statuses[position].email
            holder.binding.tvStatus.text = statuses[position].chat
            holder.binding.latandlong.text = "lat: null lon: null"
            if(statuses[position].Lat != null && statuses[position].Lon != null){
                holder.binding.latandlong.text = "lat:" + statuses[position].Lat.toString() + " lon:" + statuses[position].Lon.toString()
            }
        }

        holder.itemView.setOnClickListener {
            onItemClick!!.invoke(statuses[position])
        }
    }

    private fun setWithImage(holder: ViewHolder, position: Int) {
        holder.binding.tvUsername.text = statuses[position].email
        holder.binding.tvStatus.text = statuses[position].chat
        holder.binding.latandlong.text = "lat: null lon: null"
        if(statuses[position].Lat != null && statuses[position].Lon != null){
            holder.binding.latandlong.text = "lat:" + statuses[position].Lat.toString() + " lon:" + statuses[position].Lon.toString()
        }
        val imageReference = storageReference.child(statuses[position].imagepath!!)
        val localFile: File = File.createTempFile("tempfile",".jpg")
        try {
            imageReference.getFile(localFile).addOnSuccessListener {
                var bitmap: Bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                holder.binding.iwStatusCellImage.setImageBitmap(bitmap)
            }.addOnFailureListener(){
                holder.binding.iwStatusCellImage.setImageResource(R.drawable.ic_launcher_background)
            }
        }catch (e: IOException){
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return statuses.size
    }

    fun addStatus(status: Status){
        if(!statuses.contains(status)){
            statuses.add(status)
            allStatuses.add(status)
        }else{
            val index = statuses.indexOf(status)
            if(status.isDeleted){
                statuses.removeAt(index)
            }else {
                statuses[index] = status
            }
        }
        notifyDataSetChanged()
    }

    fun setFilteredData(filterData : ArrayList<Status>){
        this.statuses = filterData
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: ChatCellBinding):RecyclerView.ViewHolder(binding.root){
    }


}