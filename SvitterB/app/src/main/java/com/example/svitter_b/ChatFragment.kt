package com.example.svitter_b

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.svitter_b.databinding.FragmentChatBinding
import com.example.svitter_b.databinding.FragmentStatusDetailBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlin.collections.ArrayList


class ChatFragment : Fragment() {

    private lateinit var binding: FragmentChatBinding

    private lateinit var firebaseAuth: FirebaseAuth

    private val adapter = StatusAdapter()

    private lateinit var viewModel: StatusViewModel

    private lateinit var storageReference: StorageReference

    private lateinit var firebaseStorage: FirebaseStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentChatBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(StatusViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()

        firebaseStorage = FirebaseStorage.getInstance()

        storageReference = firebaseStorage.getReference("images")

        binding.RecyclerViewChat.adapter = adapter

        var statusSearch = mutableListOf<Status>()

        statusSearch.addAll(adapter.statuses)

        binding.swStatuses.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(search: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(p0: String?): Boolean {
                var filteredList = ArrayList<Status>()
                for (status: Status in adapter.allStatuses) {
                    if (status.email!!.lowercase().contains(p0!!.lowercase())) {
                        filteredList.add(status)
                    }
                }
                if (filteredList.isEmpty()) {
                    Toast.makeText(requireContext(), "No data found", Toast.LENGTH_SHORT).show()
                }
                else{
                    adapter.setFilteredData(filteredList)
                }
                return true
            }
        })

        adapter.onItemClick = {
            val statusid = it.sid
            if(statusid == null){
                Toast.makeText(requireContext(), "error on clicked item", Toast.LENGTH_SHORT).show()
            }else{
                val action = ChatFragmentDirections.actionChatFragmentToStatusDetailFragment(statusid)
                Navigation.findNavController(requireView()).navigate(action)
            }
        }

        val user = firebaseAuth.currentUser

        binding.UserVal.text = user!!.email.toString()

        binding.btnLogout.setOnClickListener {
            firebaseAuth.signOut()
            Navigation.findNavController(requireView()).navigate(R.id.action_chatFragment_to_loginFragment)
            Toast.makeText(requireContext(), "Logout is successful", Toast.LENGTH_LONG).show()
        }

        binding.btnFloat.setOnClickListener {
            AddChatFragment().show(childFragmentManager,"")
        }

        viewModel.status.observe(viewLifecycleOwner, Observer {
            adapter.addStatus(it)
        })

        viewModel.getRealtimeUpdate()

        val itemTouchHelper = ItemTouchHelper(simpleCallBack)
        itemTouchHelper.attachToRecyclerView(binding.RecyclerViewChat)
    }

    private var simpleCallBack = object : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT.or(ItemTouchHelper.RIGHT)){
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val user = firebaseAuth.currentUser
            var position = viewHolder.bindingAdapterPosition
            var currentStatus = adapter.statuses[position]

            when(direction){
                ItemTouchHelper.RIGHT -> {
                    if(user!!.email == currentStatus.email){
                        UpdateChatFragment(currentStatus).show(childFragmentManager, "")
                    }else {
                        Toast.makeText(requireContext(), "Users don't match", Toast.LENGTH_LONG).show()
                    }
                }
                ItemTouchHelper.LEFT -> {
                    if (user!!.email == currentStatus.email) {
                        AlertDialog.Builder(requireContext()).also {
                            it.setTitle("Are you sure you want to delete this status")
                            it.setPositiveButton("Yes") { dialog, which ->
                                if(currentStatus.imagepath != null) {
                                    val imageReference =
                                        storageReference.child(currentStatus.imagepath!!)
                                    imageReference.delete()
                                }
                                viewModel.deleteStatus(currentStatus)
                                binding.RecyclerViewChat.adapter?.notifyItemRemoved(position)
                                Toast.makeText(
                                    requireContext(),
                                    "Status has been removed",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                            it.setNegativeButton("No") { dialog, which ->
                                dialog.dismiss()
                            }
                        }.create().show()
                    }else{
                        Toast.makeText(requireContext(), "Users don't match", Toast.LENGTH_LONG).show()
                    }
                }
            }
            binding.RecyclerViewChat.adapter?.notifyDataSetChanged()
        }
    }
}